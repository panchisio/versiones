<!DOCTYPE html>
<html>
  <head>
    <title>My first Vue app</title>
    <script src="https://unpkg.com/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  </head>
  <body>
    <div id="app">
      {{ info }}
    </div>

    <script>
      new Vue({
        el: "#app",
        data() {
          return {
            info: "Hola"
          };
        },
        mounted() {
          axios
            .get("https://api.coindesk.com/v1/bpi/currentprice.json")
            .then((response) => (this.info = response));
        }
      });
    </script>
  </body>
</html>

