from pytz import timezone
import datetime


class Querys:    

    def obtener_reporte_GDD(fecha):

        consulta = "declare @inicio VARCHAR(255), @fin VARCHAR(255) \
        set @inicio='"+fecha+" 00:00:00.000'set @fin='"+fecha+" 23:59:00.000' \
        Select Top 1000000 Case \
        When company.comName='Pronaca' Then'202' \
        When company.comName='Garvel Product' Then'517' \
        When company.comName='Posso&Cueva' Then'253' \
        When company.comName='Cenacop' Then'325' \
        When company.comName='Granpir' Then'234' \
        When company.comName='Dapromach' Then'331' \
        When company.comName='Dismag' Then'215' \
        When company.comName='Dimmia' Then'111' \
        When company.comName='Almabi' Then'360' \
        When company.comName='Skandinar' Then'121' \
        When company.comName='Disproalza' Then'247' \
        When company.comName='Madeli' Then'128' \
        When company.comName='Ecoal' Then'373' \
        When company.comName='Disprovalles' Then'210' \
        When company.comName='Pronacnor' Then'303' \
        When company.comName='Apronam' Then'162' \
        When company.comName='Paul_Florencia' Then'168' \
        When company.comName='Discarnicos' Then'261' \
        When company.comName='H&M' Then'324' \
        When company.comName='Disanahisa' Then'323' \
        When company.comName='Patricio_Cevallos' Then'254' \
        When company.comName='ProOriente' Then'140' \
        When company.comName='Alsodi' Then'314' \
        When company.comName='Judispro' Then'239' \
        Else'#' end Emp_Codigo, \
        Descu.didCode as Id_Negociacion, \
        Descu.Tipo, \
        Descu.Cliente as Codigo_Cliente, \
        Day(Descu.Fecha) as Dia_Negociacion, \
        Month(Descu.Fecha) as Mes_Negociacion, \
        Year(Descu.Fecha) as Anio_Negociacion, \
        Ruta as Cod_Vendedor, Porcentaje as Descuento_Negociado, \
        'Si' as Descuento_Realizado, Descu.Status as Descuento_Status, \
        Case When Descu.Fecha< Ped2.Fecha Then'Si' Else'No' End as Desc_Prev_Pedido,Descu.Procesado as Descuento_Procesado_XSales, \
        Case When len(Ped2.Fecha)> 4 Then'Si' Else'No' End as Pedido_Realizado From \
        (Select  D.didCode,D.cusCode CLIENTE,Convert(Varchar,D.didSystemDate,25) FECHA, D.rotCode RUTA, \
        Replace(I.dlpMinDiscount1,',','.') PORCENTAJE, \
        Case When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%RECHAZ%' Then'Rechazado' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:"'+"SI%' ESCAPE':' Then'Aprobado' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:"'+"No%' ESCAPE':' Then'Rechazado' \
        When D.ApvCode IS NULL Then'Aprobado'When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:null%'+"' ESCAPE':' Then'Pendiente' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:"'+"EXCEPCION%' ESCAPE':' Then'Pendiente' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) IS NULL Then'Pendiente'END As STATUS, \
        Case When D.didProcess='1' Then'Si' Else'No' End As PROCESADO, \
        case when apvcode is NULL then'RANGO' else'FUERA RANGO' end as TIPO From DiscountDetailUp D \
        Inner Join DiscountDetailProductUp I On D.didCode=I.didCode \
        Where D.didCanceled='0' And D.didProcess='1' And D.DidSystemDate Between @inicio And @fin And D.didCode Not In (Select T.didCode From demandTeamProductDiscount T \
        Inner Join Demand P On T.dmdCode=P.dmdCode Where P.dmdCancelOrder='1' And P.dmdProcess IS NULL) ) Descu \
        Left Join Company On 1=1 \
        Left Join (Select  D.rotCode, D.cusCode,Max(D.dmdDate) as Fecha From Demand D Left Join DemandProduct DP On D.dmdCode= DP.dmdCode \
        Where D.dmdDate Between @inicio And @fin And docCode='ped' and D.rotCode in (Select rotCode From route Where chaCode='V01') And D.dmdCancelOrder= 0 And DP.proCode in (Select proCode From product Where cl4Code='IDGDD001') Group By D.rotCode, D.cusCode) Ped2 On Descu.cliente= Ped2.cuscode and Descu.Ruta= Ped2.rotcode \
        order by Descu.Ruta"

        return consulta
