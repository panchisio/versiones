from bs4 import BeautifulSoup
from tabulate import tabulate
import pandas as pd
import numpy as np
import os

import io
from tabulate import tabulate


class Utilidades:
    
    os.system('cls')


    def __init__(self, response ):
        self.response = response

    def esc(code):
        return f'\033[{code}m'

    def obtener_info(self):
        html_brute = self.response.text
        soup = BeautifulSoup(html_brute, "html.parser")
        fragmentList = soup.findAll("table")
        
        if len(fragmentList)>0:
            return True
        else:
            return False


    def crear_directorio(directorio, fecha):
        print("")
        print(Utilidades.esc('45'),"Fecha consulta: "+fecha,Utilidades.esc('0'))            
        try:
            os.stat(directorio)
        except:
            os.mkdir(directorio)

    def generar_archivos(path_):
        print("")
        print(Utilidades.esc('46'),"Generando archivo...",Utilidades.esc('0'))
        path = os.getcwd()
        path = path+'/'+path_
        n = os.listdir(path)

        mergedData = pd.DataFrame()
        for files in n:
            if files.endswith('.xlsx'):
                #data = pd.read_excel(files, index_col=None, converters={'Codigo_Cliente':str})
                #data = pd.read_excel(files, index_col=None)
                data = pd.read_excel(path_+"/"+files, dtype={'Emp_Codigo':np.str,
                                        'Id_Negociacion':np.str,
                                        'Tipo':np.str,
                                        'Codigo_Cliente':np.str,
                                        'Dia_Negociacion':np.str,
                                        'Mes_Negociacion':np.str,
                                        'Anio_Negociacion':np.str,
                                        'Cod_Vendedor':np.str,
                                        'Descuento_Negociado':np.str,
                                        'Descuento_Realizado':np.str,
                                        'Descuento_Status':np.str,
                                        'Desc_Prev_Pedido':np.str,
                                        'Descuento_Procesado_XSales':np.str,
                                        'Pedido_Realizado':np.str})
                data['Id_Negociacion'] = data['Id_Negociacion'].str.strip()
                data['Codigo_Cliente'] = data['Codigo_Cliente'].str.strip()
                data['Cod_Vendedor'] = data['Cod_Vendedor'].str.strip()
                data['Descuento_Negociado'] = data['Descuento_Negociado'].str.strip()
                #mergedData.columns = mergedData.columns.str.replace(' ', '_')
                mergedData = mergedData.append(data)

        mergedData.to_excel("Excel.xlsx", index=None)
        mergedData.to_csv("negogdd.txt", sep='\t', index = None)
        print(Utilidades.esc('46'),"Archivo generado con exito",Utilidades.esc('0'))
        print("Presione una tecla para finalizar...")
