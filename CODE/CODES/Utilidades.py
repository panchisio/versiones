import requests
from bs4 import BeautifulSoup

class Utilidades:

    def __init__(self, zonales):
        self.zonales = zonales

    def obtener_sesion(self):
            session = requests.Session()
            payload = {'connectionName':''+self.zonales+'_XSS_441_PRD'}
            s = session.get("https://prd1.xsalesmobile.net/"+self.zonales+"/xsm/Login/Index")
            cc = s.cookies['ASP.NET_SessionId']
            s = session.post("https://prd1.xsalesmobile.net/"+self.zonales+"/xsm/Login/validatedSession")
            s = session.post("https://prd1.xsalesmobile.net/"+self.zonales+"/xsm/Login/serverVersion")
            s = session.post("https://prd1.xsalesmobile.net/"+self.zonales+"/xsm/Login/DisplayDDListConnections")
            s = session.post("https://prd1.xsalesmobile.net/"+self.zonales+"/xsm/Login/setConnection", data=payload)
            s = session.post("https://prd1.xsalesmobile.net/"+self.zonales+"/xsm/Login/SetLanguage")

            return cc

    def logear_sesion(self, cookie, user, password):

            cookies = {
                'ASP.NET_SessionId': cookie,
            }

            headers = {
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52',
                'Accept': 'application/json, text/javascript, */*; q=0.01',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-User': '?1',
                'Sec-Fetch-Dest': 'empty',
                'Accept-Language': 'es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
                'Referer': 'https://prd1.xsalesmobile.net/'+self.zonales+'/xsm/Login/Index',
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Length': '0',
                'Origin': 'https://prd1.xsalesmobile.net',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            }

            data = {
              'connectionName': ''+self.zonales+'_XSS_441_PRD',
              'username': user,
              'password': password
            }

            response = requests.post('https://prd1.xsalesmobile.net/'+self.zonales+'/xsm/Login/userLogonServer', headers=headers, cookies=cookies, data=data)


    def obtener_event(self, cookie):

            cookies = {
                'ASP.NET_SessionId': cookie,
            }

            headers = {
                'Connection': 'keep-alive',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'navigate',
                'Sec-Fetch-User': '?1',
                'Sec-Fetch-Dest': 'document',
                'Referer': 'https://prd1.xsalesmobile.net/'+self.zonales+'/xsm/app/css/global.css?vcss=20191107',
                'Accept-Language': 'es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
                'Origin': 'https://prd1.xsalesmobile.net',
            }

            response = requests.get('https://prd1.xsalesmobile.net/'+self.zonales+'/xsm/app/webForms/webTools/sqlQuery/DBQueryUI.aspx', headers=headers, cookies=cookies)

            html_brute = response.text
            soup = BeautifulSoup(html_brute, "html.parser")

            fragmentList = soup.findAll("input")
            ff = soup.find("input", {"id": "__EVENTVALIDATION"})
            ee= ff.get('value')

            return ee

    def ejecutar_consulta(self, cookie, event, query):

            cookies = {
                'ASP.NET_SessionId': cookie,
            }

            headers = {
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Upgrade-Insecure-Requests': '1',
                'Origin': 'https://prd1.xsalesmobile.net',
                'Content-Type': 'application/x-www-form-urlencoded',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36 OPR/68.0.3618.197',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-Mode': 'navigate',
                'Sec-Fetch-User': '?1',
                'Sec-Fetch-Dest': 'document',
                'Referer': 'https://prd1.xsalesmobile.net/'+self.zonales+'/xsm/app/css/global.css?vcss=20191107',
                'Accept-Language': 'es-ES,es;q=0.9',
            }


            data = {
              '__EVENTTARGET': '',
              '__EVENTARGUMENT': '',
              '__LASTFOCUS': '',
              '__VIEWSTATE': '',
              'Ddl_BaseDatos': ''+self.zonales+'_XSS_441_PRD',
              'optradio': 'Rb_DecimalCo',
              'TxtSql': query,
              'lblBtnExecute': 'Ejecutar',
              'ddlExport': '-1',
              '__SCROLLPOSITIONX': '0',
              '__SCROLLPOSITIONY': '0',
              '__EVENTVALIDATION': event
            }

            response = requests.post('https://prd1.xsalesmobile.net/'+self.zonales+'/xsm/app/webForms/webTools/sqlQuery/DBQueryUI.aspx', headers=headers, cookies=cookies, data=data)


            html_brute = response.text
            soup = BeautifulSoup(html_brute, "html.parser")

            print(soup)

    def hablar(self):
        print("Hola",self.zonales)