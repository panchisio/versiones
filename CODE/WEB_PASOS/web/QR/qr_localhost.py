import qrcode
import socket

def obtener_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 1))  # connect() for UDP doesn't send packets
    local_ip_address = s.getsockname()[0]

    return local_ip_address



# Creamos el código QR y entre comillas simples escribimos la cadena que se va a codificar, en este caso usamos la dirección de nuestro blog
img = qrcode.make(str(obtener_ip())+':8000')

# Abrimos un archivo en modo escritura que es donde se guardará nuestro código.
img_file = open('localhost.png', 'wb')

# Guardamos nuestro código en el archivo que creamos y lo cerramos
img.save(img_file)
img_file.close()
