using Microsoft.EntityFrameworkCore;

namespace Camaro
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
        {
        }

        public virtual DbSet<Direcction> Direcction { get; set; } = null!;
        public virtual DbSet<User> User { get; set; } = null!;
    }
}