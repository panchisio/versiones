using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Camaro.Controllers
{

    [ApiController]
    [Route("/direccion")]
    public class DirecctionController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public DirecctionController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet()]

        public async Task<ActionResult<List<Direcction>>> Get()
        
        {
            return await _context.Direcction.ToListAsync();
        }
    }
}