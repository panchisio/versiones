from Turnos import Turnos
from Conexion import Conexion
from Casos import Casos
import time
import random

class Utilidades:

    def message(fecha):
        print("Seguro desea continuar con fecha: ",fecha)
        aceptar = input()

    def Turno_4am1(user, fecha):
        
        Utilidades.message(fecha)

        for x, turnos in enumerate(Turnos.Turno_4am_1()):

            stock = Conexion(turnos, user, fecha, fecha, "04:10", "04:35", Casos.Stock())
            stock.addData()

            time.sleep(0.5)

            gdd = Conexion(turnos, user, fecha, fecha, "05:10", "05:25", Casos.Modulo_GDD())
            gdd.addData()
            time.sleep(0.5)

            generacion_rutas = Conexion(turnos, user, fecha, fecha, "05:00", "05:35", Casos.Generacion_Rutas())
            generacion_rutas.addData()
            time.sleep(0.5)

            pos_gdd = Conexion(turnos, user, fecha, fecha, "04:20", "04:40", Casos.Validacion_GDD())
            pos_gdd.addData()
            time.sleep(0.5)

            

            #print(x, turnos, fecha, user)

    def Turno_4am2(user, fecha):
        
        Utilidades.message(fecha)

        for x, turnos in enumerate(Turnos.Turno_4am_2()):

            stock = Conexion(turnos, user, fecha, fecha, "04:10", "04:35", Casos.Stock())
            stock.addData()

            time.sleep(0.5)

            gdd = Conexion(turnos, user, fecha, fecha, "05:00", "05:10", Casos.Modulo_GDD())
            gdd.addData()
            time.sleep(0.5)

            generacion_rutas = Conexion(turnos, user, fecha, fecha, "05:35", "06:00", Casos.Generacion_Rutas())
            generacion_rutas.addData()
            time.sleep(0.5)

            pos_gdd = Conexion(turnos, user, fecha, fecha, "05:40", "06:00", Casos.Validacion_GDD())
            pos_gdd.addData()
            time.sleep(0.5)

            

            #print(x, turnos, fecha, user)


    def Turno_3am(user, fecha):
        
        Utilidades.message(fecha)

        for x, turnos in enumerate(Turnos.Turno_3am()):

            stock = Conexion(turnos, user, fecha, fecha, "03:15", "03:35", Casos.Stock())
            stock.addData()

            time.sleep(0.5)

            gdd = Conexion(turnos, user, fecha, fecha, "03:20", "03:40", Casos.Modulo_GDD())
            gdd.addData()
            time.sleep(0.5)

            generacion_rutas = Conexion(turnos, user, fecha, fecha, "04:00", "04:35", Casos.Generacion_Rutas())
            generacion_rutas.addData()
            time.sleep(0.5)

            pos_gdd = Conexion(turnos, user, fecha, fecha, "04:50", "05:00", Casos.Validacion_GDD())
            pos_gdd.addData()
            time.sleep(0.5)

            

            #print(x, turnos, fecha, user)

    def Sincronizacion_Total_DZ(user, fecha, zonal, rutas):
        
        Utilidades.message(fecha)

        for x, ruta in enumerate(rutas):

            hora=random.randint(18, 20)
            minuto=random.randint(10, 45)
            hora_random=str(hora)+':'+str(minuto)

            stock = Conexion(zonal, user, fecha, fecha, hora_random, hora_random, Casos.Sincronizacion_Total(ruta))
            stock.addData()

            time.sleep(0.5)

