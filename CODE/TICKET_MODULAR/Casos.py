

class Casos:

    def Stock():
        asunto ="Req: Validacion de stock - "
        descripcion ="Se realizan las revisiones del día para la compañia y el stock se encuentra recibido de manera correcta"
        resolucion="Se procede a verificar el status de las rutas y el Job de General en Xsales se ejecuto correctamente"
        modosolicitud=""
        tipocaso="Stock"
        nivel ="Primer Nivel"
        status="Cerrado"
        ruta="TODAS"
        rol="Vendedor"
        region="DZ"
        escalado="Ninguno"
        casoxsales=""

        lista = [asunto, descripcion, resolucion, modosolicitud, tipocaso, nivel, status, ruta, rol, region, escalado, casoxsales]

        return lista

    def Modulo_GDD():
        asunto ="Req:Validacion de Modulo de Negociacion - "
        descripcion ="Se realiza la Validacion del Modulo de negociación para la compañia, se encuentra activo"
        resolucion="El modulo de negociacion se encuentra activo"
        modosolicitud=""
        tipocaso="GDD"
        nivel ="Primer Nivel"
        status="Cerrado"
        ruta="TODAS"
        rol="Vendedor"
        region="DZ"
        escalado="Ninguno"
        casoxsales=""

        lista = [asunto, descripcion, resolucion, modosolicitud, tipocaso, nivel, status, ruta, rol, region, escalado, casoxsales]

        return lista

    def Generacion_Rutas():
        asunto ="Req: Validacion de generacion de rutas automaticas - "
        descripcion ="Se realizan las revisiones para comprobar que en la compañia las rutas se generen de manera automatica, tenga clientes, promociones, catalogo de productos y ninguna quede en status cero"
        resolucion="Se realiza el seguimiento y validacion hasta comprobar que toda la informacion se encuentra generada correctamente y las rutas se encuentran listas para sincronizar"
        modosolicitud=""
        tipocaso="Sincronización"
        nivel ="Primer Nivel"
        status="Cerrado"
        ruta="TODAS"
        rol="Vendedor"
        region="DZ"
        escalado="Ninguno"
        casoxsales=""

        lista = [asunto, descripcion, resolucion, modosolicitud, tipocaso, nivel, status, ruta, rol, region, escalado, casoxsales]

        return lista

    def Validacion_GDD():
        asunto ="Req:Validacion de Matriz de Negociacion - "
        descripcion ="Se realiza la Validacion de la Matriz de Negociacion Para la compañia"
        resolucion="La matriz de negociacion se encuentra cargada correctamente con vigencia actual"
        modosolicitud=""
        tipocaso="GDD"
        nivel ="Primer Nivel"
        status="Cerrado"
        ruta="TODAS"
        rol="Vendedor"
        region="DZ"
        escalado="Ninguno"
        casoxsales=""

        lista = [asunto, descripcion, resolucion, modosolicitud, tipocaso, nivel, status, ruta, rol, region, escalado, casoxsales]

        return lista

    def Sincronizacion_Total(_ruta):
        asunto ="Req: validacion de sincronizacion Total - "
        descripcion ="La ruta requiere saber si  sincronizo de manera total su gestión del día"
        resolucion="Se revisa, la ruta sincronizó correctamente"
        modosolicitud="Chat"
        tipocaso="Sincronización"
        nivel ="Primer Nivel"
        status="Cerrado"
        ruta=_ruta
        rol="Vendedor"
        region="DZ"
        escalado="Ninguno"
        casoxsales=""

        lista = [asunto, descripcion, resolucion, modosolicitud, tipocaso, nivel, status, ruta, rol, region, escalado, casoxsales]

        return lista

