
import requests
import json

class Conexion:

    def __init__(self, zonal, user, date_i, date_f, time_i, time_f, message ):
        self.zonal = zonal
        self.user = user
        self.date_i = date_i
        self.date_f = date_f
        self.time_i = time_i
        self.time_f = time_f
        self.message = message


    def printData(self):

        print(self.zonal, self.user, self.date_i, self.date_f, self.time_i, self.time_f, self.message[0])
        pass


    def addData(self):

        url = 'http://200.7.215.198:9090/updaterpronaca/datos/index.php/nuevo'
        data = {
                'modosolicitud': self.message[3],
                'tecnico': self.user,
                'tipocaso': self.message[4],
                'nivel': self.message[5],
                'status': self.message[6],
                'asunto': self.message[0] + self.zonal,
                'descripcion': self.message[1],
                'resolucion': self.message[2],
                'ruta': self.message[7],
                'rol': self.message[8],
                'region': self.message[9],
                'escalado': self.message[10],
                'casoxsales': self.message[11],
                'fecha_apertura': self.date_i,
                'hora_apertura': self.time_i,
                'fecha_cierre': self.date_f,
                'hora_cierre': self.time_f
                }
        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(url, data=json.dumps(data), headers=headers)
        print(self.message[4], self.zonal, self.date_i, r.status_code)
        #print(r.text)
        #print(r.request)


