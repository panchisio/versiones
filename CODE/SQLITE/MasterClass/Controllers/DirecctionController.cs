using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterClass.Model;

namespace MasterClass.Controllers
{

    [ApiController]
    [Route("/direccion")]
    public class DirecctionController : ControllerBase
    {
        private readonly MainContext _context;

        public DirecctionController(MainContext context)
        {
            _context = context;
        }

        [HttpGet()]

        public async Task<ActionResult<List<Direcction>>> Get()
        
        {
            return await _context.Direcction.ToListAsync();
        }
    }
}