using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MasterClass.Model;

namespace MasterClass.BackgroudTasks
{
    public class IntervalTaskHostedService : IHostedService, IDisposable
    {

        private Timer _timer = null!;

        public Task StartAsync(CancellationToken stoppingToken)
        {
            //_logger.LogInformation("Timed Hosted Service running.");

            _timer = new Timer(SendInfo, null, TimeSpan.Zero, 
                TimeSpan.FromSeconds(10));

            return Task.CompletedTask;
        }

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private void SendInfo(object? state)
        {

            using (var context = new MainContext())
            {
                Direcction direccion = new Direcction();
                direccion.Name = Summaries[Random.Shared.Next(Summaries.Length)];
                direccion.Time = Summaries[Random.Shared.Next(Summaries.Length)];

                context.Add(direccion);
                context.SaveChanges();
                 
            }

        }


        private void DoWork(object? state)
        {

            
            //var count = Interlocked.Increment(ref executionCount);
            //_logger.LogInformation("Timed Hosted Service is working. Count: {Count}", count);
            string nameFile = "a" + new Random().Next(1000) + ".txt";
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Temp", nameFile);

            File.Create(path);
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            //_logger.LogInformation("Timed Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }


        
    }
}