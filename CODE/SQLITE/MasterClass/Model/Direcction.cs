﻿using System;
using System.Collections.Generic;

namespace MasterClass.Model
{
    public partial class Direcction
    {
        public long Id { get; set; }
        public string? Name { get; set; }
        public string? Time { get; set; }
    }
}
