import imgkit

config = imgkit.config(wkhtmltoimage=r'wkhtmltox/bin/wkhtmltoimage.exe')
csspath = r"css/bootstrap.min.css"

imgkit.from_file('index.html', 'out.jpg',css=csspath, config=config)
