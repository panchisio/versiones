namespace myWebApiCore.Modelos
{
    public class Persona
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public DateTime FechaDeNacimiento { get; set; }
        public bool Activo { get; set; }
    }
}