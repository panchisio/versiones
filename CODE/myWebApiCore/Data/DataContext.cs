using Microsoft.EntityFrameworkCore;
using myWebApiCore.Modelos;

namespace myWebApiCore.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DbContext> options) : base(options)
        {
            
        }

        public DbSet <Persona> Personas { get; set; }
    }
}