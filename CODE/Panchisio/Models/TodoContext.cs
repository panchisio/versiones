using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Panchisio.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public DbSet<TodoItem> TodoItems { get; set; } = null!;
    }
}