import concurrent.futures
import numpy as np
from time import sleep
from random import randint
import logging

logging.basicConfig(level=logging.DEBUG, format='%(threadName)s: %(message)s')

numbers = [10,23,54,7,89,100]
def get_max_number(numbers):
    greatest_num = np.max(numbers)
    sleep(randint(0, 10))
    logging.info("Greatest number is :{}".format(greatest_num))
    #print("Greatest number is :{}".format(greatest_num))

with concurrent.futures.ThreadPoolExecutor(max_workers = 3) as executor:
    thread1 = executor.submit(get_max_number, (numbers))
    thread1 = executor.submit(get_max_number, (numbers))
    thread1 = executor.submit(get_max_number, (numbers))
    thread1 = executor.submit(get_max_number, (numbers))
    thread1 = executor.submit(get_max_number, (numbers))
    thread1 = executor.submit(get_max_number, (numbers))
    #print("Thread 1 executed ? :",thread1.done())
    #sleep(2)
#print("Thread 1 executed ? :",thread1.done())