from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
import numpy as np
from time import sleep

import logging

logging.basicConfig(level=logging.DEBUG, format='%(threadName)s: %(message)s')

def log(n):
    log_value = np.log(n)
    sleep(1)
    #return log_value
    print(log_value)
    #logging.info("Log value is :{}".format(log_value))

if __name__ == '__main__':

    values = [77777,10,100,1000,1,999999,1010, 3333, 777]
    
    with ThreadPoolExecutor(max_workers = 3) as executor:
        thread1 = executor.map(log, values)

    #for result in thread1:
    #    print(np.round(result,2))

#fifa