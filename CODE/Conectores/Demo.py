# example of pinging the status of a set of websites asynchronously
from urllib.request import urlopen
from urllib.error import URLError
from urllib.error import HTTPError
from http import HTTPStatus
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
 

#funcion pintado

def esc(code):
    return f'\033[{code}m'


# get the status of a website
def get_website_status(url):
    # handle connection errors
    try:
        # open a connection to the server with a timeout
        with urlopen(url, timeout=3) as connection:
            # get the response code, e.g. 200
            code = connection.getcode()
            return code
    except HTTPError as e:
        return e.code
    except URLError as e:
        return e.reason
    except:
        return e
 
# interpret an HTTP response code into a status
def get_status(code):
    if code == HTTPStatus.OK:
        return 'OK'
    return 'ERROR'
 
# check status of a list of websites
def check_status_urls(urls):
    # create the thread pool
    with ThreadPoolExecutor(len(urls)) as executor:
        # submit each task, create a mapping of futures to urls
        future_to_url = {executor.submit(get_website_status, url):url for url in urls}
        # get results as they are available
        for future in as_completed(future_to_url):
            # get the url for the future
            url = future_to_url[future]

            # get the status for the website

            code = future.result()

            # interpret the status
            status = get_status(code)

            if status == 'ERROR':
                print(esc('41'),f'{url:20s}\t{status:5s}\t{code}',esc('0'))
            else:
                print(esc('42'),f'{url:20s}\t{status:5s}\t{code}',esc('0'))

                


            
            
            # report status
            #print(esc('41'), 'Selecciona una Opcion', esc('0'))
            
 
# list of urls to check
URLS = ['http://sgr-logistica.tia.com.ec:8080/webui/',
        'http://sgr-locales.tia.com.ec:8080/webui/',
        'http://sgr-compras.tia.com.ec:8080/webui/',
        'http://redmine.tia.com.ec/',
        'https://reclutamiento.tia.com.ec/',
        'http://mesadeservicios.tia.com.ec/ServiceTonic/login.jsf',
        'http://10.122.6.97:8080/login?from=%2F',
        'http://intranet.tia.com.ec/intra/',
        'http://git.tia.com.ec:8001/users/sign_in',
        'http://facturacion.tia.com.ec/login.jsf',
        'https://eva.tia.com.ec/login/index.php',
        'https://infocorporativo.tia.com.ec/user/login',
        'http://132.142.160.115:8161/admin/queues.jsp',
        'http://132.142.160.187:8080/pentaho/Login',
        'http://132.142.160.159:8080/Avante/']
# check all urls
check_status_urls(URLS)