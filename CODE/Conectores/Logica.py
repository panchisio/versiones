import requests
from lxml import html
from bs4 import BeautifulSoup
from Utilidades import Utilidades

class Logica:

    def __init__(self, url_base, password, nombre):
        self.url_base = url_base
        self.password = password
        self.nombre = nombre


    def getResponse(self):
        headers = {
            'Connection': 'keep-alive',
            'Authorization': 'Basic '+self.password,
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36 Edg/95.0.1020.53',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language': 'es-419,es;q=0.9,es-ES;q=0.8,en;q=0.7,en-GB;q=0.6,en-US;q=0.5',
        }

        response = requests.get(self.url_base, headers=headers, verify=False)

        return response

    def getStatus(self):

        try:

            tree = html.fromstring(self.getResponse().text)
            list_status = tree.xpath('//*[@id="collapse1"]/div/div[1]/div/form/p/text()')
            status_server = str(list_status[0]).strip()

            return status_server
        except Exception as inst:

            return inst

    def mostrarStatus(self):
        substring = "Started"

        if self.getStatus().find(substring) != -1:
            print(Utilidades.esc('42'),self.nombre+"-"+self.getStatus(),Utilidades.esc('0')+" ")
        else:
            print(Utilidades.esc('41'),self.nombre+"-"+self.getStatus(),Utilidades.esc('0')+" ")

    def detenerTarea(self):
        substring = "Started"

        if self.getStatus().find(substring) != -1:
            #si encuentra la cadena de servivor iniciado detiene
            #print(Utilidades.esc('42'),self.nombre+"-"+self.getStatus(),Utilidades.esc('0')+" ")
            print("Detener Tarea "+self.nombre)
            headers = {
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Authorization': 'Basic '+self.password,
                'Upgrade-Insecure-Requests': '1',
                'Origin': 'http://xserpc.xsalesmobile.net:8080',
                'Content-Type': 'application/x-www-form-urlencoded',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36 Edg/95.0.1020.53',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Referer': self.url_base,
                'Accept-Language': 'es-419,es;q=0.9,es-ES;q=0.8,en;q=0.7,en-GB;q=0.6,en-US;q=0.5',
            }

            data = {
            'action': 'stop',
            'route': '',
            'JobListExcute': 'ALL',
            'start': ''
            }

            response = requests.post(self.url_base, headers=headers, data=data, verify=False)
            print(response.status_code)
        else:
            #print(Utilidades.esc('41'),self.nombre+"-"+self.getStatus(),Utilidades.esc('0')+" ")
            #si no encuentra la cadena de servivor iniciado entonces no hace nada
            print("Nada que hacer")

    def iniciarTarea(self):
        substring = "Started"

        if self.getStatus().find(substring) != -1:
            #si encuentra la cadena de servivor iniciado no hace nada
            #print(Utilidades.esc('42'),self.nombre+"-"+self.getStatus(),Utilidades.esc('0')+" ")
            print("Nada que hacer")
        else:
            #print(Utilidades.esc('41'),self.nombre+"-"+self.getStatus(),Utilidades.esc('0')+" ")
            #si no encuentra la cadena de servivor iniciado entonces la inicia
            print("Iniciar Tarea: "+self.nombre)

            headers = {
                'Connection': 'keep-alive',
                'Cache-Control': 'max-age=0',
                'Authorization': 'Basic '+self.password,
                'Upgrade-Insecure-Requests': '1',
                'Origin': 'http://xserpc.xsalesmobile.net:8080',
                'Content-Type': 'application/x-www-form-urlencoded',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36 Edg/95.0.1020.53',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Referer': self.url_base,
                'Accept-Language': 'es-419,es;q=0.9,es-ES;q=0.8,en;q=0.7,en-GB;q=0.6,en-US;q=0.5',
            }

            data = {
            'action': 'start',
            'route': '',
            'JobListExcute': 'ALL',
            'start': ''
            }

            response = requests.post(self.url_base, headers=headers, data=data, verify=False)

            print(response.status_code)


        

