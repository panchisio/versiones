class Zonales:

    def zonales():
        zonales = {
        'DISANAHISA_XSS':'323',
        'PAUL_FLORENCIA_XSS':'168',
        'PROORIENTE_XSS':'140',
        'MADELI_XSS':'128',
        'DISMAG_XSS':'215',
        'JUDISPRO_XSS':'239',
        'GARVELPRODUCT_XSS':'517',
        'DISPROALZA_XSS':'247',
        'ALSODI_XSS':'314',
        'DISPROVALLES_XSS':'210',
        'DAPROMACH_XSS':'330',
        'CENACOP_XSS':'325',
        'POSSO_CUEVA_XSS':'253',
        'ALMABI_XSS':'360',
        'GRAMPIR_XSS':'234',
        'DIMMIA_XSS':'111',
        'PATRICIO_CEVALLOS_XSS':'254',
        'SKANDINAR_XSS':'121',
        'PRONACNOR_XSS':'303',
        'H_M_XSS':'324',
        'APRONAM_XSS':'162',
        'DISCARNICOS_XSS':'261',
        'ECOAL_XSS':'373'    
        }

        return zonales

    def directa():

        directa = {
        }

        return directa
