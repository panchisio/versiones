import requests
import imgkit
from datetime import datetime

import time



# current date and time
#now = datetime.now()

#timestamp = datetime.timestamp(now)
#print("timestamp =", timestamp)
def main():
    
    cookies = {
    'OCTOPUS7c41f8ff3dc705c3be6991ac8e9f56949ef35f61': 'e0E5MTU1MjdFLTVBN0YtNDVCRC1CMjRFLUVDQjZDODIyM0IxQn0%3D',
    }

    headers = {
    'Connection': 'keep-alive',
    'sec-ch-ua': '"Microsoft Edge";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
    'Accept': 'text/html, */*; q=0.01',
    'X-Requested-With': 'XMLHttpRequest',
    'sec-ch-ua-mobile': '?0',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36 Edg/95.0.1020.38',
    'sec-ch-ua-platform': '"Windows"',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Dest': 'empty',
    'Referer': 'https://monitoreo-tia.tia.com.ec/sensors.htm?id=0&filter_status=5',
    'Accept-Language': 'es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    }

    params = (
    ('id', '0'),
    ('filter_status', '5'),
    ('_hjax', 'true'),
    ('_', '1635607899270'),
    )

    response = requests.get('https://monitoreo-tia.tia.com.ec/sensors.htm', headers=headers, params=params, cookies=cookies)

    now = datetime.now()

    timestamp = datetime.timestamp(now)


    config = imgkit.config(wkhtmltoimage=r'wkhtmltox/bin/wkhtmltoimage.exe')
    #ok = imgkit.from_string('Hello!', 'out.jpg',config=config)
    ok = imgkit.from_string(response.text, str(timestamp)+'.jpg',config=config)





while(True):
    try:
        main()
    except:
        print("Error")
    time.sleep(480)
    
