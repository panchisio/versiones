import requests
import imgkit
from datetime import datetime

import time



# current date and time
#now = datetime.now()

#timestamp = datetime.timestamp(now)
#print("timestamp =", timestamp)
def main():
    
    cookies = {
        'VtexRCMacIdv7': 'e1beead0-9c7e-11eb-9ac0-077e105f4714',
        'VtexFingerPrint': 'dbb6bb97ceee498f4e173468db6c5981',
        '_ga': 'GA1.3.628335884.1618335441',
        '_fbp': 'fb.2.1618335443671.855362712',
        '_hjid': '21b4d1f7-94aa-476e-bb6f-f070ba3121ba',
        '_gcl_au': '1.1.1010438107.1630108693',
        'fanplayr': '%7B%22uuid%22%3A%221618335446371-d1983d9d59941fdd8e5be1b1%22%2C%22uk%22%3A%225.hlnCc9izV5UgXsI7Qb6.1618335514%22%2C%22sk%22%3A%2224c129ad49906a4a9d37d61421661a66%22%2C%22se%22%3A%22e1.fanplayr.com%22%2C%22tm%22%3A1%2C%22t%22%3A1630108965341%7D',
        'cto_bundle': 'ycztFF9FYmtNT1owNkZ2SlJkVmh4UDMzZWxFMyUyRkpIOVdFaGViRFRSUDA0WHhCdEJkc2N6RnM3bVRZWEFVdWNHOHNLMFZFRXF6UWRBVXBnVHduekxLcUYyc2RqSUZPSU1ja0h5bFpZaGQ1OTlvZ3B5bGRVelpPVlJoTEdOcG9pZkRidzBrTng0emhKbDJJTFgxamVwTlFQVkh1QSUzRCUzRA',
        'OCTOPUS7c41f8ff3dc705c3be6991ac8e9f56949ef35f61': 'e0QwMzZCNTQ3LTEwRjEtNDI2Mi1CQTY2LTBEMTNFNjNBQzE4Rn0%3D',
    }

    headers = {
        'Connection': 'keep-alive',
        'sec-ch-ua': '"Microsoft Edge";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
        'Accept': 'text/html, */*; q=0.01',
        'X-Requested-With': 'XMLHttpRequest',
        'sec-ch-ua-mobile': '?1',
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Mobile Safari/537.36 Edg/95.0.1020.30',
        'sec-ch-ua-platform': '"Android"',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'https://monitoreo-tia.tia.com.ec/sensors.htm?id=0&filter_status=5',
        'Accept-Language': 'es,es-ES;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    }

    params = (
        ('id', '0'),
        ('filter_status', '5'),
        ('_hjax', 'true'),
        ('_', '1635535596468'),
    )

    response = requests.get('https://monitoreo-tia.tia.com.ec/sensors.htm', headers=headers, params=params, cookies=cookies)

    now = datetime.now()

    timestamp = datetime.timestamp(now)


    config = imgkit.config(wkhtmltoimage=r'wkhtmltox/bin/wkhtmltoimage.exe')
    #ok = imgkit.from_string('Hello!', 'out.jpg',config=config)
    ok = imgkit.from_string(response.text, str(timestamp)+'.jpg',config=config)





while(True):
    try:
        main()
    except:
        print("Error")
    time.sleep(480)
    
