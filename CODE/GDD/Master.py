from modulos_.Companias import Companias
from modulos_.Controller import Controller
from modulos_.Querys import Querys
from concurrent.futures import ThreadPoolExecutor
from modulos_.Utilidades import Utilidades
from datetime import datetime
#import datetime


class Master:

    def iniciar_GDD(fecha):
        try:
            now = datetime.now() 
            directorio = "GDD-"+now.strftime("%Y-%m-%d-%H-%M-%S")
            Utilidades.crear_directorio(directorio, fecha)

            #generamos proceso consulta y descarga cambiar fecha año-mes-dia 2021-12-02
            with ThreadPoolExecutor(max_workers = 3) as executor:
                thread1 = executor.map(Controller.main_controller,
                Companias.lista_Companias(Querys.obtener_reporte_GDD(fecha), directorio))

            #generamos proceso generacion archivos
            Utilidades.generar_archivos(directorio)
            input()

        except Exception as inst:
            print(Utilidades.esc('41'),"Error: "+str(inst),Utilidades.esc('0'))

    def consulta_master(lista_compania):
        try:
            now = datetime.now()    
            print(Utilidades.esc('45'),"última captura: ",now,Utilidades.esc('0'),'\n')

            with ThreadPoolExecutor(max_workers = 3) as executor:
                thread1 = executor.map(Controller.cvs_controller,lista_compania)
        except Exception as inst:
            print(Utilidades.esc('41'),"Error: "+str(inst),Utilidades.esc('0'))
        

    

    def consulta_vista(query):
        now = datetime.now()   
        print(Utilidades.esc('45'),"última captura: ",now,Utilidades.esc('0'),'\n')
        with ThreadPoolExecutor(max_workers = 3) as executor:
                thread1 = executor.map(Controller.cvs_controller,
                Companias.lista_Companias(query, ""))

    

    def consulta_vista_zonal(zonal, query):
        try:
            now = datetime.now()    
            print(Utilidades.esc('45'),"última captura: ",now,Utilidades.esc('0'),'\n')

            lista = []
            lista.append(Companias(zonal,"Soporte.nuo","Nuo2021*",query,""))

            with ThreadPoolExecutor(max_workers = 3) as executor:
                    thread1 = executor.map(Controller.cvs_controller,
                    lista)
        except Exception as inst:
            print(Utilidades.esc('41'),"Error: "+str(inst),Utilidades.esc('0'))


    def revisiones_diarias(lista_companias):
        try:
            now = datetime.now()
            print(Utilidades.esc('45'),"última captura: ",now,Utilidades.esc('0'),'\n') 
            #generamos proceso consulta y descarga cambiar fecha año-mes-dia 2021-12-02
            with ThreadPoolExecutor(max_workers = 3) as executor:
                thread1 = executor.map(Controller.main_controller,lista_companias)

            #generamos proceso generacion archivos
            #Utilidades.generar_archivos(directorio)
            #input()

        except Exception as inst:
            print(Utilidades.esc('41'),"Error: "+str(inst),Utilidades.esc('0'))


