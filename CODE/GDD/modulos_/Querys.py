from pytz import timezone
import datetime


class Querys:    

    def obtener_rutas_activas():

        consulta = "select top 30 rotCode, gnlstatus, gnlstatusdate from general"

        return consulta

    def obtener_reporte_GDD(fecha):

        consulta = "declare @inicio VARCHAR(255), @fin VARCHAR(255) \
        set @inicio='"+fecha+" 00:00:00.000'set @fin='"+fecha+" 23:59:00.000' \
        Select Top 1000000 Case \
        When company.comName='Pronaca' Then'202' \
        When company.comName='Garvel Product' Then'517' \
        When company.comName='Posso&Cueva' Then'253' \
        When company.comName='Cenacop' Then'325' \
        When company.comName='Granpir' Then'234' \
        When company.comName='Dapromach' Then'331' \
        When company.comName='Dismag' Then'215' \
        When company.comName='Dimmia' Then'111' \
        When company.comName='Almabi' Then'360' \
        When company.comName='Skandinar' Then'121' \
        When company.comName='Disproalza' Then'247' \
        When company.comName='Madeli' Then'128' \
        When company.comName='Ecoal' Then'373' \
        When company.comName='Disprovalles' Then'210' \
        When company.comName='Pronacnor' Then'303' \
        When company.comName='Apronam' Then'162' \
        When company.comName='Paul_Florencia' Then'168' \
        When company.comName='Discarnicos' Then'261' \
        When company.comName='H&M' Then'324' \
        When company.comName='Disanahisa' Then'323' \
        When company.comName='Patricio_Cevallos' Then'254' \
        When company.comName='ProOriente' Then'140' \
        When company.comName='Alsodi' Then'314' \
        When company.comName='Judispro' Then'239' \
        Else'#' end Emp_Codigo, \
        Descu.didCode as Id_Negociacion, \
        Descu.Tipo, \
        Descu.Cliente as Codigo_Cliente, \
        Day(Descu.Fecha) as Dia_Negociacion, \
        Month(Descu.Fecha) as Mes_Negociacion, \
        Year(Descu.Fecha) as Anio_Negociacion, \
        Ruta as Cod_Vendedor, Porcentaje as Descuento_Negociado, \
        'Si' as Descuento_Realizado, Descu.Status as Descuento_Status, \
        Case When Descu.Fecha< Ped2.Fecha Then'Si' Else'No' End as Desc_Prev_Pedido,Descu.Procesado as Descuento_Procesado_XSales, \
        Case When len(Ped2.Fecha)> 4 Then'Si' Else'No' End as Pedido_Realizado From \
        (Select  D.didCode,D.cusCode CLIENTE,Convert(Varchar,D.didSystemDate,25) FECHA, D.rotCode RUTA, \
        Replace(I.dlpMinDiscount1,',','.') PORCENTAJE, \
        Case When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%RECHAZ%' Then'Rechazado' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:"'+"SI%' ESCAPE':' Then'Aprobado' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:"'+"No%' ESCAPE':' Then'Rechazado' \
        When D.ApvCode IS NULL Then'Aprobado'When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:null%'+"' ESCAPE':' Then'Pendiente' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) Like'%Respuesta"+'"%:"'+"EXCEPCION%' ESCAPE':' Then'Pendiente' \
        When (Select A.ApvResponseDetail From Approval A Where A.apvCode=D.apvCode) IS NULL Then'Pendiente'END As STATUS, \
        Case When D.didProcess='1' Then'Si' Else'No' End As PROCESADO, \
        case when apvcode is NULL then'RANGO' else'FUERA RANGO' end as TIPO From DiscountDetailUp D \
        Inner Join DiscountDetailProductUp I On D.didCode=I.didCode \
        Where D.didCanceled='0' And D.didProcess='1' And D.DidSystemDate Between @inicio And @fin And D.didCode Not In (Select T.didCode From demandTeamProductDiscount T \
        Inner Join Demand P On T.dmdCode=P.dmdCode Where P.dmdCancelOrder='1' And P.dmdProcess IS NULL) ) Descu \
        Left Join Company On 1=1 \
        Left Join (Select  D.rotCode, D.cusCode,Max(D.dmdDate) as Fecha From Demand D Left Join DemandProduct DP On D.dmdCode= DP.dmdCode \
        Where D.dmdDate Between @inicio And @fin And docCode='ped' and D.rotCode in (Select rotCode From route Where chaCode='V01') And D.dmdCancelOrder= 0 And DP.proCode in (Select proCode From product Where cl4Code='IDGDD001') Group By D.rotCode, D.cusCode) Ped2 On Descu.cliente= Ped2.cuscode and Descu.Ruta= Ped2.rotcode \
        order by Descu.Ruta"

        return consulta

    def obtener_gdd_hoy():
        current_time = datetime.datetime.now(timezone('America/Bogota'))
        dia = current_time.strftime("%d")
        mes = current_time.strftime("%m")
        anio = current_time.strftime("%Y")
        fecha = anio+'-'+mes+'-'+dia

        consulta = "select top 10000 didCreatedOn,didProcess,didCanceled,cuscode,apvcode,didcode, rotcode from discountDetailUp where convert(date,didCreatedOn,101)='"+fecha+"' and didProcess='0' and didCanceled='0'"

        return consulta

    def obtener_gdd_otros(fecha):
        #año-mes-dia

        consulta = "select top 10000 didCreatedOn,didProcess,didCanceled,cuscode,apvcode,didcode, rotcode from discountDetailUp where convert(date,didCreatedOn,101)='"+fecha+"' and didProcess='0' and didCanceled='0'"


        return consulta


    def obtener_clientes_hoy(fecha):
        #año-mes-dia

        consulta = "select top(200) \
                    CONVERT(varchar,cusSystemDate,10) as Fecha, \
                    rotcode as Ruta, \
                    cusCode as Codigo, \
                    custaxid1 Identificación, \
                    cusname as Cliente, \
                    cusBusinessName as RazonSocial, \
                    _processMessage Estado \
                    from customerup where convert(datetime,CUSSYSTEMDATE,10)='"+fecha+"' and cusnew='true'"


        return consulta


    def obtener_gdd_no_procesados(fecha):
        #año-mes-dia

        consulta = "select top 10000 didCreatedOn Fecha_Ingreso,didProcess, \
                    didCanceled,cuscode,apvcode,didcode, rotcode,_processMessage,_processDate \
                    from discountDetailUp where convert(date,didCreatedOn,101)='"+fecha+"' \
                    and didProcess='0' and didCanceled='0'"

        return consulta

    def obtener_gdd_no_procesados_didcode_apvcode(fecha):
        #año-mes-dia

        consulta = "select top 10000 didcode, apvcode \
                    from discountDetailUp where convert(date,didCreatedOn,101)='"+fecha+"' \
                    and didProcess='0' and didCanceled='0'"

        return consulta

    def validar_cliente(identificacion):
        consulta = "SELECT TOP 100 CUSCODE FROM CUSTOMER WHERE CUSTAXID1 LIKE"+"'%"+identificacion+"%'"""

        return consulta

    def validar_stock():
        consulta ="declare @name VARCHAR(20), @preventa date, @despacho date, @horainicio datetime, @horafin datetime \
            select @name= (select  top(1) comname from company) \
            select @preventa= (select  top(1) exvDateValue from dbo.extendedTableValue3 where exfFieldName='x_aptTransactionDate') \
            select @despacho= (select  top(1) exvDateValue from dbo.extendedTableValue3 where exfFieldName='x_aptServerLastUpdate') \
            select @horainicio=(SELECT max(Trndate) FROM[TRANSACTION] WHERE TRNSCRIPTS LIKE'%CALCULATE%') \
            select  @horafin=(SELECT max(trnLastDate) FROM[TRANSACTION] WHERE TRNSCRIPTS LIKE'%CALCULATE%') \
            select @name as Zonal, @preventa as STOCK_PREVENTA, @despacho as STOCK_DESPACHO, dateadd(hh,-1,@horainicio) as HoraECUInicioStock,dateadd(hh,-1,@horafin) as HoraECUFinStock"

        return consulta

    def validar_dz_dia():

        consulta = "declare @preventa date, @despacho date, @horainicio datetime, @horafin datetime, @customerxss int, @cusdelete int, @customerroute int, @cusst int, @customerstatus int, @customerroute1 int, @product int, @catalog int,@cataa int,@catai int, @promotion int, @promotiond int, @promotiondp int, @generico int, @cpa int, @pa int \
            select @preventa= (select  top(1) exvDateValue from dbo.extendedTableValue3 where exfFieldName='x_aptTransactionDate') \
            select @despacho= (select  top(1) exvDateValue from dbo.extendedTableValue3 where exfFieldName='x_aptServerLastUpdate') \
            select @horainicio=(SELECT max(Trndate) FROM[TRANSACTION] WHERE TRNSCRIPTS LIKE'%CALCULATE%') \
            select  @horafin=(SELECT max(trnLastDate) FROM[TRANSACTION] WHERE TRNSCRIPTS LIKE'%CALCULATE%') \
            select @customerxss= (select count(*) from customer) \
            select @cusdelete=(select count(*) from customer where _Deleted='0' ) \
            select @customerroute=(select count(distinct cuscode) from customerroute where ctrvisittoday='1' and rotcode not like'%t%') \
            select @customerstatus=(select count(*) from customerstatus WHERE CUSCODE IN (SELECT CUSCODE FROM CUSTOMERROUTE)) \
            select @cusst=(select count(*) from customerstatus where cuscode in(select cusCode from customerroute where ctrVisitToday='1' and rotcode not like'%t%')) \
            select @customerroute1=(select count(distinct cuscode) from customerroute) \
            select @product=(select count(*) from product where _Deleted='0') \
            select @catalog=(select count(*) from CatalogDetail where catCode like'%NacDZ%') \
            select @cataa=(select count(*) from Catalog where catActive='1') \
            select @catai=(select count(*) from Catalog where _deleted='1') \
            select @promotion=(select count(*) from promotion) \
            select @promotiond=(select count(*) from promotiondetail) \
            select @promotiondp=(select count(*) from promotiondetailproduct) \
            select @generico=(select count(*) from customerstatus where cusCode like'%099999999%') \
            select @cpa=(select count(*) from customerpartner) \
            select @pa=(select count(*) from partner) \
            select @preventa as PREVENTA \
            , @despacho as DESPACHO, dateadd(hh,-1,@horainicio) as ECInicioStock,dateadd(hh,-1,@horafin) as ECFinStock \
            , @customerxss as CUSTOMER_XSS, @cusdelete as CUSTOMER_DELETE_0 \
            , @customerroute1 as CUSROUT_TOTAL,@customerroute as CUSROUTE_VISIT_TODAY,@cusst as CUSSTATUS_VISIT_TODAY, @customerstatus as CUSSTATUS_TOTAL \
            , @product as PRODUCT, @catalog as CATALOGO, @cataa as CA, @catai as CI \
            , @promotion as PROMOTION, @promotiond as PROMOTIOND, @promotiondp as PROMOTIONDP, @generico as GENERICO, @cpa CUSPAR, @pa PAR"

        return consulta