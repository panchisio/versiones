class Companias:

    def __init__(self, zonal, user, password, query, path):
        self.zonal = zonal
        self.user = user
        self.password = password
        self.query = query
        self.path = path



    def lista_Companias(query, path):
        lista = []
        lista.append(Companias("PRONACA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("MADELI","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISANAHISA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PAUL_FLORENCIA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PROORIENTE","Soporte.nuo","Nuo2021*",query ,path))        
        lista.append(Companias("DISMAG","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("JUDISPRO","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("GARVELPRODUCT","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISPROALZA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("ALSODI","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISPROVALLES","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DAPROMACH","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("CENACOP","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("POSSO_CUEVA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("ALMABI","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("GRAMPIR","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PRONACNOR","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DIMMIA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("SKANDINAR","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PATRICIO_CEVALLOS","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISCARNICOS","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("APRONAM","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("ECOAL","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("H_M","Soporte.nuo","Nuo2021*",query ,path))
        
        return lista

    def lista_Compania(zonal, query):
            lista = []
            lista.append(Companias(zonal,"Soporte.nuo","Nuo2021*",query ,""))
            
            return lista

    def lista_Compania_Pronaca(query, path):
        lista = []
        lista.append(Companias("PRONACA","Soporte.nuo","Nuo2021*",query ,path))

        return lista

    def lista_Compania_3am(query, path):
        lista = []
        lista.append(Companias("PRONACA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("CENACOP","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DAPROMACH","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISPROALZA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("MADELI","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PAUL_FLORENCIA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISPROVALLES","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("POSSO_CUEVA","Soporte.nuo","Nuo2021*",query ,path))  

        return lista

    def lista_Compania_4am_1(query, path):
        lista = []
        lista.append(Companias("GARVELPRODUCT","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PROORIENTE","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("ALMABI","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISMAG","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("GRAMPIR","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("ALSODI","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISANAHISA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("JUDISPRO","Soporte.nuo","Nuo2021*",query ,path))
        
        return lista

    def lista_Compania_4am_2(query, path):
        lista = []
        lista.append(Companias("DIMMIA","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PATRICIO_CEVALLOS","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("SKANDINAR","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("PRONACNOR","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("H_M","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("APRONAM","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("DISCARNICOS","Soporte.nuo","Nuo2021*",query ,path))
        lista.append(Companias("ECOAL","Soporte.nuo","Nuo2021*",query ,path))
        
        return lista
